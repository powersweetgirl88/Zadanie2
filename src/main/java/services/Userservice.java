package services;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import dto.User;
import io.restassured.filter.Filter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.given;

 public  class Userservice {
     private OpenApiValidationFilter filter = new OpenApiValidationFilter("https://petstore.swagger.io/v2/swagger.json");
     private RequestSpecification spec;


     public Userservice(){
            spec = given()
                    .baseUri("https://petstore.swagger.io/v2")
                    .filter(filter)
                    .log().all()
                    .contentType(ContentType.JSON);

     }
     public Response CreateUser(User user){
         return given(spec)
                .body(user)
                .when()
                .post("/user");

    }
    public Response GetUserByName(String name){
        return given(spec)

                .when()
                .get("/user/"+name);


    }
}
