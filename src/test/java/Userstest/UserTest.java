package Userstest;

import dto.User;
import dto.UserOut;
import generators.UserGenerator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import services.Userservice;

public class UserTest {
    @Test
    public void createUserTest(){
        User user = UserGenerator.getNewUser();
        Userservice userservice = new Userservice();
        Response response = userservice.CreateUser(user);
       UserOut actual= response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);
       UserOut expected = UserOut.builder()
               .code(200L)
               .message(user.getId().toString())
               .type("unknown")
               .build();
        Assertions.assertEquals(expected,actual);}
@ParameterizedTest
@ValueSource(strings = {"admin", "moderator", "user"})
 public void getUserByName(String name){
        Userservice userService = new Userservice();
Response response=userService.GetUserByName(name);
    UserOut actual= response.then()
            .log().all()
            .assertThat()
            .statusCode(404)
            .and()
            .extract()
            .body()
            .as(UserOut.class);
    UserOut expected = UserOut.builder()
            .code(1L)
            .message("User not found")
            .type("error")
            .build();
    Assertions.assertEquals(expected,actual);}}
